package helpdesk.pagination;

import helpdesk.authorization.fragments.AuthorizationFragment;
import helpdesk.common.helpers.CacheHelper;
import helpdesk.common.helpers.CookieHelper;
import helpdesk.common.helpers.HttpHelper;
import helpdesk.common.helpers.PropertyHelper;
import helpdesk.logout.fragments.LogoutFragment;
import helpdesk.pagination.fragments.PaginationFragment;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import us.abstracta.jmeter.javadsl.core.TestPlanStats;
import us.abstracta.jmeter.javadsl.core.engines.EmbeddedJmeterEngine;

import java.io.IOException;
import java.util.Properties;

import static us.abstracta.jmeter.javadsl.JmeterDsl.*;

public class RamiPaginationTest {
    EmbeddedJmeterEngine embeddedJmeterEngine = new EmbeddedJmeterEngine();
    Properties properties = new Properties();

    @BeforeTest
    private void init() throws IOException {
        properties = PropertyHelper.ReadProperties();
        PropertyHelper.SetPropertiesToEngine(embeddedJmeterEngine, properties);

    }

    @Test(testName = "RamiPaginationTest")
    private void Test() throws IOException {

        TestPlanStats run = testPlan(
                HttpHelper.getDefaults(),
                CacheHelper.getCache(),
                CookieHelper.getCookie(),

                threadGroup(
                        "Debug", 1, 1,
                        new AuthorizationFragment("admin", "admindev").get(),
                        new PaginationFragment().get(),
                        new LogoutFragment("admin").get()
                ),
                debugPostProcessor(),
                resultsTreeVisualizer()
        ).run();
    }
}
