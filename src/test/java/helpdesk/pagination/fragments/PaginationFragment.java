package helpdesk.pagination.fragments;

import org.apache.jmeter.protocol.http.util.HTTPConstants;
import us.abstracta.jmeter.javadsl.core.controllers.DslSimpleController;

import java.time.Duration;

import static us.abstracta.jmeter.javadsl.JmeterDsl.*;

public class PaginationFragment {
    private final String url = "eyJmaWx0ZXJpbmciOiB7InN0YXR1c19faW4iOiBbMSwgMl19LCAic29ydGluZyI6ICJjcmVhdGVkIiwgInNlYXJjaF9zdHJpbmciOiAiIiwgInNvcnRyZXZlcnNlIjogZmFsc2V9";
    private final String numberTicket = "10";
    public PaginationFragment() {}

    public DslSimpleController get() {
        return simpleController(
                "TF_PAGINATION",
                httpSampler("TICKETS_PAGE", "/tickets/")
                        .method(HTTPConstants.GET),
                httpSampler("PAGINATION_TICKETS(" + numberTicket + ")", "/datatables_ticket_list/" + url)
                        .method(HTTPConstants.GET)
                        .rawParam("length", numberTicket),
                uniformRandomTimer(Duration.ofSeconds(1), Duration.ofSeconds(5))
        );
    }
}
