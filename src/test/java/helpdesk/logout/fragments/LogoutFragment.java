package helpdesk.logout.fragments;

import org.apache.jmeter.protocol.http.util.HTTPConstants;
import us.abstracta.jmeter.javadsl.core.controllers.DslSimpleController;

import java.time.Duration;

import static us.abstracta.jmeter.javadsl.JmeterDsl.*;

public class LogoutFragment {
    private final String userName;
    public LogoutFragment(String userName) {
        this.userName = userName;
    }

    public DslSimpleController get() {
        return simpleController( "TF_LOGOUT " + userName,
                httpSampler("MAIN_PAGE", "/")
                        .method(HTTPConstants.GET),
                httpSampler("LOGOUT", "/logout/"),
                uniformRandomTimer(Duration.ofSeconds(1), Duration.ofSeconds(5))
        );
    }
}
