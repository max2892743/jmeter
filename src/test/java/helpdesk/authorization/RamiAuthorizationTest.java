package helpdesk.authorization;

import helpdesk.authorization.fragments.AuthorizationFragment;
import helpdesk.common.helpers.CacheHelper;
import helpdesk.common.helpers.CookieHelper;
import helpdesk.common.helpers.HttpHelper;
import helpdesk.create_user.fragments.CreateUserFragment;
import helpdesk.logout.fragments.LogoutFragment;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import us.abstracta.jmeter.javadsl.core.TestPlanStats;

import java.io.IOException;
import java.time.Duration;

import static us.abstracta.jmeter.javadsl.JmeterDsl.*;
import static us.abstracta.jmeter.javadsl.wrapper.WrapperJmeterDsl.testElement;

public class RamiAuthorizationTest {
    @BeforeTest
    private void init() {

    }

    @Test(testName = "RamiAuthorizationTest")
    private void Test() throws IOException {
        TestPlanStats run = testPlan(
            HttpHelper.getDefaults(),
            CacheHelper.getCache(),
            CookieHelper.getCookie(),

            threadGroup(
                    "Debug", 1, 1,
                    new AuthorizationFragment("admin", "admindev").get()
            ),
            resultsTreeVisualizer()

        ).run();
    }
}
