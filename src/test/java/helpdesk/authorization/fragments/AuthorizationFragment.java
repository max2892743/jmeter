package helpdesk.authorization.fragments;

import org.apache.jmeter.protocol.http.util.HTTPConstants;
import us.abstracta.jmeter.javadsl.core.controllers.DslSimpleController;

import java.time.Duration;

import static us.abstracta.jmeter.javadsl.JmeterDsl.*;

public class AuthorizationFragment {
    private final String userName;
    private final String userPassword;

    public AuthorizationFragment(String userName, String userPassword) {
        this.userName = userName;
        this.userPassword = userPassword;
    }

    public DslSimpleController get() {
        return simpleController(
                "TF_AUTHORIZATION " + userName,
                httpSampler("MAIN_PAGE", "/")
                        .method(HTTPConstants.GET),
                httpSampler("LOGIN_PAGE", "/login/?next=/")
                        .method(HTTPConstants.GET)
                        .children(
                                regexExtractor("CSRF_LOGIN", "n\" value=\"(.*)\">")
                                        .template("$1$")
                                        .matchNumber(1)
                                        .defaultValue("CSRF_ERROR")
                        ),
                httpSampler("LOGIN_PAGE", "/login/")
                        .method(HTTPConstants.POST)
                        .rawParam("username", userName)
                        .rawParam("password", userPassword)
                        .rawParam("next", "/")
                        .rawParam("csrfmiddlewaretoken", "${CSRF_LOGIN}"),
                uniformRandomTimer(Duration.ofSeconds(1), Duration.ofSeconds(5))
        );
    }
}
