package helpdesk.create_ticket;

import helpdesk.authorization.fragments.AuthorizationFragment;
import helpdesk.common.helpers.CacheHelper;
import helpdesk.common.helpers.CookieHelper;
import helpdesk.common.helpers.HttpHelper;
import helpdesk.create_ticket.fragments.CreateTicketFragment;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import us.abstracta.jmeter.javadsl.core.TestPlanStats;

import java.io.IOException;

import static us.abstracta.jmeter.javadsl.JmeterDsl.*;

public class RamiCreateTicketTest {
    @BeforeTest
    private void init() {}

    @Test(testName = "RamiCreateTicketTest")
    private void Test() throws IOException {
        TestPlanStats run = testPlan(
                HttpHelper.getDefaults(),
                CacheHelper.getCache(),
                CookieHelper.getCookie(),

                threadGroup(
                        "Debug", 1, 1,
                        new AuthorizationFragment("admin", "admindev").get(),
                        new CreateTicketFragment("admin").get()
                ),
                resultsTreeVisualizer()
        ).run();
    }
}
