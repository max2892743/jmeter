package helpdesk.create_ticket.fragments;

import org.apache.jmeter.protocol.http.util.HTTPConstants;
import us.abstracta.jmeter.javadsl.core.controllers.DslSimpleController;

import java.time.Duration;

import static us.abstracta.jmeter.javadsl.JmeterDsl.*;

public class CreateTicketFragment {
    private final String userName;

    public CreateTicketFragment(String userName) {
        this.userName = userName;
    }

    public DslSimpleController get() {
        return simpleController(
                "TF_CREATE_TICKET" + userName,
                httpSampler("SUBMIT_PAGE", "/tickets/submit/")
                        .method(HTTPConstants.GET)
                        .children(
                                regexExtractor("CSRF_ADD_TICKETS", "n\" value=\"(.*)\">")
                                        .template("$1$")
                                        .matchNumber(1)
                                        .defaultValue("CSRF_ERROR")
                        ),
                httpSampler("SUBMIT_TICKET", "/tickets/submit/")
                        .method(HTTPConstants.POST)
                        .rawParam("csrfmiddlewaretoken", "${CSRF_ADD_TICKETS}")
                        .rawParam("queue", "2")
                        .rawParam("title", "123")
                        .rawParam("body", "sdfa")
                        .rawParam("priority", "3")
                        .rawParam("due_date", "2023-11-15 00:00:00")
                        .rawParam("attachment", "")
                        .rawParam("submitter_email", "max@gmail.com")
                        .rawParam("assigned_to", "3")
                ,
                uniformRandomTimer(Duration.ofSeconds(1), Duration.ofSeconds(5))
        );
    }
}
