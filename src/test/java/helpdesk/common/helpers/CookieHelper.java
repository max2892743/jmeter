package helpdesk.common.helpers;

import us.abstracta.jmeter.javadsl.http.DslCookieManager;

import static us.abstracta.jmeter.javadsl.JmeterDsl.httpCookies;

public class CookieHelper {
    public static DslCookieManager getCookie() {
        return httpCookies()
                .clearCookiesBetweenIterations(true);
    }
}
