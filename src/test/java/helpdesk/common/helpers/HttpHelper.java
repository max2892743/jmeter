package helpdesk.common.helpers;

import us.abstracta.jmeter.javadsl.http.DslHttpDefaults;

import java.nio.charset.StandardCharsets;

public class HttpHelper {
    public static DslHttpDefaults getDefaults() {
        return new DslHttpDefaults()
                .protocol("${__P(PROTOCOL)}")
                .host("${__P(HOST)}")
                .port(23232)
                .encoding(StandardCharsets.UTF_8);
    }
}
