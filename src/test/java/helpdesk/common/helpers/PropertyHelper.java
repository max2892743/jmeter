package helpdesk.common.helpers;

import us.abstracta.jmeter.javadsl.core.engines.EmbeddedJmeterEngine;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertyHelper {
    static public Properties ReadFileProperties(String path) throws IOException {
        Properties properties = new Properties();
        FileInputStream fileInputStream = new FileInputStream(path);
        properties.load(fileInputStream);
        return properties;
    }

    static public Properties ReadProperties() throws IOException {
        String pathCommon = "src/test/resources/test_data_csv/common/common.properties";

        Properties commonProperties = ReadFileProperties(pathCommon);

        Properties properties = new Properties();
        properties.setProperty("PROTOCOL", System.getProperty("PROTOCOL", commonProperties.getProperty("PROTOCOL", "http")));
        properties.setProperty("HOST", System.getProperty("HOST", commonProperties.getProperty("HOST", "localhost")));
        return properties;
    }

    static public void SetPropertiesToEngine(EmbeddedJmeterEngine engine, Properties properties) {
        properties.forEach(
                (k, v) ->  engine
                        .prop(k.toString(), v)
        );
    }
}
