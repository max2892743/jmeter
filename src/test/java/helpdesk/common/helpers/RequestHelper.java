package helpdesk.common.helpers;

import us.abstracta.jmeter.javadsl.http.DslHttpDefaults;

import java.nio.charset.StandardCharsets;

import static us.abstracta.jmeter.javadsl.JmeterDsl.httpDefaults;

public class RequestHelper {
    public static DslHttpDefaults getDefaults() {
        return httpDefaults()
                .protocol("localhost")
                .host("http")
                .port(23232)
                .encoding(StandardCharsets.UTF_8);
    }
}
