package helpdesk.create_user;

import helpdesk.authorization.fragments.AuthorizationFragment;
import helpdesk.common.helpers.CacheHelper;
import helpdesk.common.helpers.CookieHelper;
import helpdesk.common.helpers.HttpHelper;
import helpdesk.create_user.fragments.CreateUserFragment;
import helpdesk.logout.fragments.LogoutFragment;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import us.abstracta.jmeter.javadsl.core.TestPlanStats;

import java.io.IOException;

import static us.abstracta.jmeter.javadsl.JmeterDsl.*;

public class RamiCreateUserTest {
    @BeforeTest
    private void init() {}

    @Test(testName = "RamiCreateUserTest")
    private void Test() throws IOException {
        TestPlanStats run = testPlan(
                HttpHelper.getDefaults(),
                CacheHelper.getCache(),
                CookieHelper.getCookie(),

                threadGroup(
                        "Debug", 1, 1,
                        new AuthorizationFragment("admin", "admindev").get(),
                        new CreateUserFragment("max", "dsFe23412").get(),
                        new LogoutFragment("admin").get()
                ),

                resultsTreeVisualizer()
        ).run();
    }
}
