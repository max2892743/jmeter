package helpdesk.create_user.fragments;

import org.apache.jmeter.protocol.http.util.HTTPConstants;
import us.abstracta.jmeter.javadsl.core.controllers.DslSimpleController;
import us.abstracta.jmeter.javadsl.jdbc.DslJdbcSampler;

import java.sql.Types;
import java.time.Duration;

import static us.abstracta.jmeter.javadsl.JmeterDsl.*;
import static us.abstracta.jmeter.javadsl.jdbc.JdbcJmeterDsl.jdbcSampler;

public class CreateUserFragment {
    private final String userName;
    private final String userPasswrod;

    public CreateUserFragment(String userName, String userPasswrod) {
        this.userName = userName;
        this.userPasswrod = userPasswrod;
    }

    public DslSimpleController get() {
        return simpleController(
          "TF_CREATE_USER_FRAGMENT " + userName,
                httpSampler("SYSTEM_SETTINGS_PAGE", "/admin/auth/user/")
                        .method(HTTPConstants.GET),
                httpSampler("USERS_PAGE", "/admin/auth/user/")
                        .method(HTTPConstants.GET),
                httpSampler("OPEN_CREATE_USER_PAGE", "/admin/auth/user/add/")
                        .method(HTTPConstants.GET)
                        .children(
                                regexExtractor("CSRF_LOGIN", "n\" value=\"(.*)\">")
                                        .template("$1$")
                                        .matchNumber(1)
                                        .defaultValue("CSRF_ERROR")
                        ),
                httpSampler("CREATE_USER", "/admin/auth/user/add/")
                        .method(HTTPConstants.POST)
                        .rawParam("csrfmiddlewaretoken", "${CSRF_LOGIN}")
                        .rawParam("username", userName)
                        .rawParam("password_1", userPasswrod)
                        .rawParam("password_2", userPasswrod)
                        .rawParam("_save", "Save"),
                httpSampler("USER_CHANGE_RIGHTS_PAGE", "/admin/auth/user/3/change/")
                        .method(HTTPConstants.GET)
                        .children(
                                regexExtractor("CSRF_CHANGE_USER", "n\" value=\"(.*)\">")
                                        .template("$1$")
                                        .matchNumber(1)
                                        .defaultValue("CSRF_USER_ERROR")
                        ),
                httpSampler("USER_CHANGE_RIGHTS", "/admin/auth/user/3/change/")
                        .method(HTTPConstants.POST)
                        .rawParam("csrfmiddlewaretoken", "${CSRF_CHANGE_USER}")
                        .rawParam("username", "max")
                        .rawParam("first_name", "")
                        .rawParam("last_name", "")
                        .rawParam("email", "")
                        .rawParam("is_active", "on")
                        .rawParam("is_staff", "on")
                        .rawParam("last_login_0", "2023-11-14")
                        .rawParam("last_login_1", "17:08:22")
                        .rawParam("date_joined_0", "2023-11-13")
                        .rawParam("date_joined_1", "17:05:47")
                        .rawParam("initial-date_joined_0", "2023-11-13")
                        .rawParam("initial-date_joined_1", "17:05:47")
                        .rawParam("_save", "Save"),
                uniformRandomTimer(Duration.ofSeconds(1), Duration.ofSeconds(5))
        );
    }
}
